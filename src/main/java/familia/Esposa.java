package familia;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ESPOSA")
public class Esposa {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private int id;
	
	@Column(name = "NOMBRE")
	private String nombre;
	
	@JoinColumn(name = "ID_HIJO")
	private Hijo hijo;
	
	@OneToOne(mappedBy = "esposa")
	private Esposo esposo;

	public Esposo getEsposo() {
		return esposo;
	}



	public void setEsposo(Esposo esposo) {
		this.esposo = esposo;
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getNombre() {
		return nombre;
	}



	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	@Override
	public String toString() {
		return "Esposa [id=" + id + ", nombre=" + nombre + "]";
	}



	



	
	

}
