package familia;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ESPOSO")
public class Esposo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private int id;
	
	@Column(name = "NOMBRE")
	private String nombre;

	@JoinColumn(name = "ID_ESPOSA")
	@OneToOne
	private Esposa esposa;
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Esposa getEsposa() {
		return esposa;
	}


	public void setEsposa(Esposa esposa) {
		this.esposa = esposa;
	}


	@Override
	public String toString() {
		return "Esposo [id=" + id + ", nombre=" + nombre + ", esposa=" + esposa + "]";
	}


}

