package familia;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "HIJO")
@NamedQueries({
	@NamedQuery(name = "buscar.hijo", query = "SELECT a.esposa.esposo FROM Hijo a WHERE a.nombre =:nombre")
})
public class Hijo {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private int id;
	
	@Column(name = "NOMBRE")
	private String nombre;
	
	@OneToOne(mappedBy = "hijo")
	private Esposa esposa;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	
	public Esposa getEsposa() {
		return esposa;
	}

	public void setEsposa(Esposa esposa) {
		this.esposa = esposa;
	}

	@Override
	public String toString() {
		return "Hijo [id=" + id + ", nombre=" + nombre + ", esposa=" + esposa + "]";
	}

	
	
	
	
	
}
