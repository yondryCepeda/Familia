package main;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import familia.Esposa;
import familia.Esposo;
import familia.Hijo;

public class BuscarFamilia {

	public String metodo(String nombre){
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("ready");
		EntityManager em = emf.createEntityManager();
		
		Esposo h = em.createNamedQuery("buscar.hijo", Esposo.class)
				.setParameter("nombre", nombre)
				.getSingleResult();
		
		return h.getNombre();
		
	}
	public static void main(String[] args){
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("ready");
		EntityManager em = emf.createEntityManager();
		
		BuscarFamilia bf = new BuscarFamilia();
		System.out.println(bf.metodo("alex"));
	}
}
