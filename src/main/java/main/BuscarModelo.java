package main;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import entity.Carro;
import entity.Motor;

public class BuscarModelo {

	public String buscarModelo(String nombre) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("ready");
		EntityManager em = emf.createEntityManager();

		Motor motor = em.createNamedQuery("buscar.nombre", Motor.class)
		.setParameter("nombre", nombre)
		.getSingleResult();
			return motor.getCarro().getModelo();
	}

	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("ready");
		EntityManager em = emf.createEntityManager();

		BuscarModelo busqueda = new BuscarModelo();
		System.out.println(busqueda.buscarModelo("f 750"));
	}

}
