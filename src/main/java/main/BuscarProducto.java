package main;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import producto.Informacion_producto;
import producto.Producto;

public class BuscarProducto {

	public String miMetodo(String nombre){
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("ready");
		EntityManager em = emf.createEntityManager();
		
		Producto p = em.createNamedQuery("buscar.descripcion", Producto.class)
				.setParameter("nombre", nombre)
				.getSingleResult();
		
		return p.getIp().getDescripcion();
	}
	public static void main(String[] args){
		
		BuscarProducto bp = new BuscarProducto();
		System.out.println(bp.miMetodo("arroz"));
		
	}
}
